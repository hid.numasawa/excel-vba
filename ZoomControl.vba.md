### 全てのシートを[zoom]% に設定する

	Public Sub ZoomAll(zoom As Long)

	    ' 現在のアクティブを保管する
	    Dim C As Worksheet
	    Set C = ActiveSheet
	    
	    ' 全てのシートを選択する
	    Worksheets.Select
	    ActiveWindow.zoom = zoom
	    
	    ' 最初の選択状態に戻す
	    C.Select

	End Sub

### 現在のシートを[zoom]% に設定する

	Public Sub zoom(zoom As Long)
	    ActiveWindow.zoom = zoom
	End Sub

### 現在のシートを[zoom=100]% に設定する

	Public Sub Reset()
	    Call zoom(100)
	End Sub

### 全てのシートを[zoom=100]% に設定する

	Public Sub ResetAll()
	    Call ZoomAll(100)
	End Sub

### 現在のシートを[zoom=200]% に設定する

	Public Sub Zoom_200()
	    Call zoom(200)
	End Sub

### 全てのシートを[zoom=200]% に設定する

	Public Sub ZoomAll_200()
	    Call ZoomAll(200)
	End Sub

### 現在のシートを[zoom=150]% に設定する

	Public Sub Zoom_150()
	    Call zoom(150)
	End Sub

### 全てのシートを[zoom=150]% に設定する

	Public Sub ZoomAll_150()
	    Call ZoomAll(150)
	End Sub

### 現在のシートを[zoom=75]% に設定する

	Public Sub Zoom_75()
	    Call zoom(75)
	End Sub

### 全てのシートを[zoom=75]% に設定する

	Public Sub ZoomAll_75()
	    Call ZoomAll(75)
	End Sub

### 現在のシートを[zoom=66]% に設定する

	Public Sub Zoom_66()
	    Call zoom(66)
	End Sub

### 全てのシートを[zoom=66]% に設定する

	Public Sub ZoomAll_66()
	    Call ZoomAll(66)
	End Sub

### 現在のシートを[zoom=50]% に設定する

	Public Sub Zoom_50()
	    Call zoom(50)
	End Sub

### 全てのシートを[zoom=50]% に設定する

	Public Sub ZoomAll_50()
	    Call ZoomAll(50)
	End Sub



