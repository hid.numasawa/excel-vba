### 全てのシートの枠線(DisplayGridlines)を [display] の値に設定する

    ' True  : 表示
    ' False : 非標示
    Private Sub ChangeAll(display As Boolean)
    
        ' 現在のアクティブを保管する
        Dim C As Worksheet
        Set C = ActiveSheet
        
        ' 全てのシートを選択する
        Worksheets.Select
        ActiveWindow.DisplayGridlines = display
        
        ' 最初の選択状態に戻す
        C.Select
    End Sub
    
### 現在のシートの枠線(DisplayGridlines)を [display] の値に設定する

    ' True  : 表示
    ' False : 非標示
    Private Sub Change(display As Boolean)
        ActiveWindow.DisplayGridlines = display
    End Sub

### 現在のシートの枠線(DisplayGridlines)を [display=False] の値に設定する

    Public Sub Hide()
        Call Change(False)
    End Sub

### 全てのシートの枠線(DisplayGridlines)を [display=False] の値に設定する

    Public Sub HideAll()
        Call ChangeAll(False)
    End Sub

### 現在のシートの枠線(DisplayGridlines)を [display=True] の値に設定する
    Public Sub Show()
        Call Change(True)
    End Sub

### 全てのシートの枠線(DisplayGridlines)を [display=True] の値に設定する

    Public Sub ShowAll()
        Call ChangeAll(True)
    End Sub

