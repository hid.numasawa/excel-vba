### 先頭シートのセル選択をリセットする（アクティブ化）

    Public Sub Reset()
        Worksheets(1).Activate
        Call ResetCell
    End Sub

### カレントシートのセル選択をリセットする

    Public Sub ResetCell()
        Range("A1").Select
    End Sub

### 全てのシートのセル選択をリセットする（アクティブ化）

    Public Sub ResetAll()
    
        ' 現在のアクティブを保管する
        Dim C As Worksheet
        Set C = ActiveSheet
        
        ' 全てのシートを選択する
        Worksheets.Select
        Range("A1").Select
        
        ' 最初の選択状態に戻す
        C.Select
    End Sub

