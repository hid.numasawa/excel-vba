
### 現在のセルのフォントを[FontName]にする

	Public Sub FontName(FontName As String)
	    ActiveCell.Font.name = FontName
	End Sub

### 現在のシート（入力済み）のフォントを[FontName]にする

	Public Sub FontNameSheet(FontName As String)
	    Range("A1", Range("A1").SpecialCells(xlLastCell)).Font.name = FontName
	End Sub

### 全てのシート（入力済み）のフォントを[FontName]にする

	Public Sub FontNameAll(FontName As String)
	    
	    ' 現在のアクティブを保管する
	    Dim C As Worksheet
	    Set C = ActiveSheet
	    
	    ' 全てのシートを選択する
	    Dim S As Worksheet
	    For Each S In Worksheets
	        S.Activate
	        Call FontNameSheet(FontName)
	    Next S
	    
	    ' 最初の選択状態に戻す
	    C.Activate
	End Sub

### 現在のセルのフォントを[FontName=ＭＳ Ｐゴシック]にする

	Public Sub FontName_PG()
	    Call FontName("ＭＳ Ｐゴシック")
	End Sub

### 現在のシート（入力済み）のフォントを[FontName=ＭＳ Ｐゴシック]にする

	Public Sub FontNameSheet_PG()
	    Call FontNameSheet("ＭＳ Ｐゴシック")
	End Sub

### 全てのシート（入力済み）のフォントを[FontName=ＭＳ Ｐゴシック]にする

	Public Sub FontNameAll_PG()
	    Call FontNameAll("ＭＳ Ｐゴシック")
	End Sub

### 現在のセルのフォントを[FontName=ＭＳ ゴシック]にする

	Public Sub FontName_G()
	    Call FontName("ＭＳ ゴシック")
	End Sub

### 現在のシート（入力済み）のフォントを[FontName=ＭＳ ゴシック]にする

	Public Sub FontNameSheet_G()
	    Call FontNameSheet("ＭＳ ゴシック")
	End Sub

### 全てのシート（入力済み）のフォントを[FontName=ＭＳ ゴシック]にする

	Public Sub FontNameAll_G()
	    Call FontNameAll("ＭＳ ゴシック")
	End Sub

### 現在のセルのフォントを[FontName=ＭＳ Ｐ明朝]にする

	Public Sub FontName_PM()
	    Call FontName("ＭＳ Ｐ明朝")
	End Sub

### 現在のシート（入力済み）のフォントを[FontName=ＭＳ Ｐ明朝]にする

	Public Sub FontNameSheet_PM()
	    Call FontNameSheet("ＭＳ Ｐ明朝")
	End Sub

### 全てのシート（入力済み）のフォントを[FontName=ＭＳ Ｐ明朝]にする

	Public Sub FontNameAll_PM()
	    Call FontNameAll("ＭＳ Ｐ明朝")
	End Sub

### 現在のセルのフォントを[FontName=ＭＳ 明朝]にする

	Public Sub FontName_M()
	    Call FontName("ＭＳ 明朝")
	End Sub

### 現在のシート（入力済み）のフォントを[FontName=ＭＳ 明朝]にする

	Public Sub FontNameSheet_M()
	    Call FontNameSheet("ＭＳ 明朝")
	End Sub

### 全てのシート（入力済み）のフォントを[FontName=ＭＳ 明朝]にする

	Public Sub FontNameAll_M()
	    Call FontNameAll("ＭＳ 明朝")
	End Sub

### 現在のセルのフォントサイズを[FontSize]にする

	Public Sub FontSize(FontSize As Long)
	    ActiveCell.Font.size = FontSize
	End Sub

### 現在のシート（入力済み）のフォントサイズを[FontSize]にする

	Public Sub FontSizeSheet(FontSize As Long)
	    Range("A1", Range("A1").SpecialCells(xlLastCell)).Font.size = FontSize
	End Sub

### 全てのシート（入力済み）のフォントサイズを[FontSize]にする

	Public Sub FontSizeAll(FontSize As Long)
	    
	    ' 現在のアクティブを保管する
	    Dim C As Worksheet
	    Set C = ActiveSheet
	    
	    ' 全てのシートを選択する
	    Dim S As Worksheet
	    For Each S In Worksheets
	        S.Activate
	        Call FontSizeSheet(FontSize)
	    Next S
	    
	    ' 最初の選択状態に戻す
	    C.Activate
	End Sub

### 現在のセルのフォントサイズを[FontSize=6]にする

	Public Sub FontSize_6(FontSize As Long)
	    Call FontSize(6)
	End Sub

### 現在のシート（入力済み）のフォントサイズを[FontSize=6]にする

	Public Sub FontSizeSheet_6()
	    Call FontSizeSheet(6)
	End Sub

### 全てのシート（入力済み）のフォントサイズを[FontSize=6]にする

	Public Sub FontSizeAll_6()
	    Call FontSizeAll(6)
	End Sub

### 現在のセルのフォントサイズを[FontSize=8]にする

	Public Sub FontSize_8(FontSize As Long)
	    Call FontSize(8)
	End Sub

### 現在のシート（入力済み）のフォントサイズを[FontSize=8]にする

	Public Sub FontSizeSheet_8()
	    Call FontSizeSheet(8)
	End Sub

### 全てのシート（入力済み）のフォントサイズを[FontSize=8]にする

	Public Sub FontSizeAll_8()
	    Call FontSizeAll(8)
	End Sub

### 現在のセルのフォントサイズを[FontSize=10]にする

	Public Sub FontSize_10(FontSize As Long)
	    Call FontSize(10)
	End Sub

### 現在のシート（入力済み）のフォントサイズを[FontSize=10]にする

	Public Sub FontSizeSheet_10()
	    Call FontSizeSheet(10)
	End Sub

### 全てのシート（入力済み）のフォントサイズを[FontSize=10]にする

	Public Sub FontSizeAll_10()
	    Call FontSizeAll(10)
	End Sub

### 現在のセルのフォントサイズを[FontSize=12]にする

	Public Sub FontSize_12(FontSize As Long)
	    Call FontSize(12)
	End Sub

### 現在のシート（入力済み）のフォントサイズを[FontSize=12]にする

	Public Sub FontSizeSheet_12()
	    Call FontSizeSheet(12)
	End Sub

### 全てのシート（入力済み）のフォントサイズを[FontSize=12]にする

	Public Sub FontSizeAll_12()
	    Call FontSizeAll(12)
	End Sub

### 現在のセルのフォントサイズを[FontSize=14]にする

	Public Sub FontSize_14(FontSize As Long)
	    Call FontSize(14)
	End Sub

### 現在のシート（入力済み）のフォントサイズを[FontSize=14]にする

	Public Sub FontSizeSheet_14()
	    Call FontSizeSheet(14)
	End Sub

### 全てのシート（入力済み）のフォントサイズを[FontSize=14]にする

	Public Sub FontSizeAll_14()
	    Call FontSizeAll(14)
	End Sub

### 現在のセルのフォントサイズを[FontSize=16]にする

	Public Sub FontSize_16(FontSize As Long)
	    Call FontSize(16)
	End Sub

### 現在のシート（入力済み）のフォントサイズを[FontSize=16]にする

	Public Sub FontSizeSheet_16()
	    Call FontSizeSheet(16)
	End Sub

### 全てのシート（入力済み）のフォントサイズを[FontSize=16]にする

	Public Sub FontSizeAll_16()
	    Call FontSizeAll(16)
	End Sub

### 現在のセルのフォントサイズを[FontSize=18]にする

	Public Sub FontSize_18(FontSize As Long)
	    Call FontSize(18)
	End Sub

### 現在のシート（入力済み）のフォントサイズを[FontSize=18]にする

	Public Sub FontSizeSheet_18()
	    Call FontSizeSheet(18)
	End Sub

### 全てのシート（入力済み）のフォントサイズを[FontSize=18]にする

	Public Sub FontSizeAll_18()
	    Call FontSizeAll(18)
	End Sub

### 現在のセルのフォントサイズを[FontSize=20]にする

	Public Sub FontSize_20(FontSize As Long)
	    Call FontSize(20)
	End Sub

### 現在のシート（入力済み）のフォントサイズを[FontSize=20]にする

	Public Sub FontSizeSheet_20()
	    Call FontSizeSheet(20)
	End Sub

### 全てのシート（入力済み）のフォントサイズを[FontSize=20]にする

	Public Sub FontSizeAll_20()
	    Call FontSizeAll(20)
	End Sub

