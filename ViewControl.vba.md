### 全てのビューの設定を [view] に変更する

    ' xlNormalView      : 標準
    ' xlPageBreakPreview: 改ページビュー
    ' xlPageLayoutView  : ページレイアウト
    Private Sub ChangeAll(view As Integer)
    
        ' 現在のアクティブを保管する
        Dim C As Worksheet
        Set C = ActiveSheet
        
        ' 全てのシートを選択する
        Worksheets.Select
        ActiveWindow.view = view
        
        ' 最初の選択状態に戻す
        C.Select
    End Sub
    
### 現在のビューの設定を [view] に変更する

    ' xlNormalView      : 標準
    ' xlPageBreakPreview: 改ページビュー
    ' xlPageLayoutView  : ページレイアウト
    Private Sub Change(view As Integer)
        ActiveWindow.view = view
    End Sub
    
### 現在のシートのビューの設定を [view=xlNormalView] に変更する

    Public Sub Normal()
        Call Change(xlNormalView)
    End Sub
    
### 全てのシートのビューの設定を [view=xlNormalView] に変更する

    Public Sub NormalAll()
        Call ChangeAll(xlNormalView)
    End Sub

### 現在のシートのビューの設定を [view=xlPageBreakPreview] に変更する

    Public Sub PageBreak()
        Call Change(xlPageBreakPreview)
    End Sub

### 全てのシートのビューの設定を [view=xlPageBreakPreview] に変更する

    Public Sub PageBreakAll()
        Call ChangeAll(xlPageBreakPreview)
    End Sub

### 現在のシートのビューの設定を [view=xlPageLayoutView] に変更する

    Public Sub PageLayout()
        Call Change(xlPageLayoutView)
    End Sub
    
### 全てのシートのビューの設定を [view=xlPageLayoutView] に変更する

    Public Sub PageLayoutAll()
        Call ChangeAll(xlPageLayoutView)
    End Sub
